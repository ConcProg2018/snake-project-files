package au.edu.unisa.snake.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import au.edu.unisa.snake.Server;

public class Snake extends Entity implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7867038279472723808L;
	private String username;
	private int length;
	private int score;
	private Direction direction = Direction.STOP;
	private ConcurrentLinkedQueue<Location> tail;
	private boolean player = false;
	private boolean hasMoved = true;
	
	public Snake(Location location, String username) {
		super(location);
		this.username = username;
		length = 3;
		score = 0;
		tail = new ConcurrentLinkedQueue<Location>();
		tail.offer(new Location(location, 0,0));
		tail.offer(new Location(location, 0,0));
		tail.offer(new Location(location, 0,0));
	}

	public Snake(Location location, String username, boolean isPlayer) {
		this(location, username);
		this.setPlayer(true);
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}


	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @param length the length to add to
	 */
	public void addLength(int length) {
		this.length += length;
		Location last = tail.peek();
		ConcurrentLinkedQueue<Location> temp = new ConcurrentLinkedQueue<>(tail);
		tail.clear();
		for (int i = 0; i < length; i++) {
			tail.offer(new Location(last,0,0));
		}
		tail.addAll(temp);
		
	}


	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @param score the score to add to
	 */
	public void addScore(int score) {
		this.score += score;
	}

	public boolean move() {
		Direction dir = getDirection();
		int xOffset = 0;
		int yOffset = 0;
		
		switch(dir) {
		case UP:
			yOffset =- 1;
			break;
		case DOWN:
			yOffset = 1;
			break;
		case LEFT:
			xOffset =- 1;
			break;
		case RIGHT:
			xOffset = 1;
			break;
		case STOP:
			xOffset = 0;
			yOffset = 0;
			break;
		default:
			throw new EnumConstantNotPresentException(Direction.class, String.format("Direction: %s", dir));
		}
		
		if (xOffset != 0 || yOffset != 0) {
			tail.poll();
			tail.offer(new Location(getLocation(),0,0));
		}
		
		Location loc = new Location (getLocation(), xOffset, yOffset);
		if(loc.getX() < 0 || loc.getY() < 0 || loc.getX() > Server.getGameboardSize() || loc.getY() > Server.getGameboardSize()) {
			Server.eventListener.onSnakeDieEvent(this);
			return false;
		}
		Entity ent = Server.isLocationFree(loc);
		if (ent == null || ent.equals(this)) {
			if (tail.contains(loc)) {
				Server.eventListener.onSnakeDieEvent(this);
				return false;
			}
			setLocation(loc);
			return true;
		} else if (ent instanceof Fruit) {
			Server.eventListener.onSnakeEatEvent(this, (Fruit) ent);
			setLocation(loc);
			return true;
		} else {
			Server.eventListener.onSnakeDieEvent(this);
			return false;
		}
	}

	public void eat(Fruit fruit) {
		this.addScore(fruit.getScore());
		this.addLength(fruit.getScore());
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	public Direction getDirection() {
		return direction;
	}


	public void setDirection(Direction previousMove) {
		this.direction = previousMove;
	}
	

	

	/**
	 * @return the player
	 */
	public boolean isPlayer() {
		return player;
	}


	/**
	 * @param player the player to set
	 */
	public void setPlayer(boolean player) {
		this.player = player;
	}


	/**
	 * @return the tail
	 */
	public Queue<Location> getTail() {
		return tail;
	}

	@Override
	public void run() {
		if (Thread.currentThread().isInterrupted()) {
			try {
				Thread.currentThread().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return;
		}
		if (!isAlive()) {
			Thread.currentThread().interrupt();
			return;
		}
		if (!isPlayer()) {
			double d = Math.random();

			if (d < 0.2) {
				Random random = new Random();
				ArrayList<Direction> dirs = new ArrayList<Direction>(Arrays.asList(Direction.values()));
				dirs.remove(Direction.STOP);
				if (this.direction != Direction.STOP) {
					dirs.remove(this.direction);
					dirs.remove(this.direction.Opposite());
				}
				Direction dir = dirs.get(random.nextInt(dirs.size()));

				Server.eventListener.onSnakeChangeDirectionEvent(this, dir);
			}
		}
		
		if (getDirection() != Direction.STOP) {
			move();
		}
		hasMoved = true;
		
		if (Thread.currentThread().isInterrupted()) {
			return;
		}
		try {
			Thread.sleep(100L);
			run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @return the hasMoved
	 */
	public boolean isHasMoved() {
		return hasMoved;
	}

	/**
	 * @param hasMoved the hasMoved to set
	 */
	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

}
