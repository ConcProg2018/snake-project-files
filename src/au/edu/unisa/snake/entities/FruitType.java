package au.edu.unisa.snake.entities;

public enum FruitType {
	APPLE(1), ORANGE(2), STRAWBERRY(3);
	private final int value;

	private FruitType(int value) {
		this.value = value;
	}
	
/**
 * @author saketpopi
 * 
 * @return score value
 */
	public int getValue() {       
		return value;      
	} 
}
