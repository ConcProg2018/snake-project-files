package au.edu.unisa.snake.entities;

import java.io.Serializable;
import java.util.UUID;



public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7350171189456255628L;
	private final UUID uuid;
	private String username;
	private String password;
	private Snake snake;

	public User(String username, String password){
		super();
		this.uuid = UUID.randomUUID();
		this.username = username;
		this.password = password;
		this.snake = null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public UUID getUUID() {
		return uuid;
	}
	
	

	public Snake getSnake() {
		return snake;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [uuid=" + uuid + ", username=" + username + ", password=" + password + "]";
	}	
	

	
}
