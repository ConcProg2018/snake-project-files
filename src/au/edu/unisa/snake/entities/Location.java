package au.edu.unisa.snake.entities;

import java.io.Serializable;

/**
 * The Location class is a variable for use with an Entity
 * It stores an Entities X and Y value in a gameboard 
 * @author mattshinkfield - shimy021
 *
 */
public class Location implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7667043149335483910L;

	private int x; //horizontal value
	private int y; //vertical value


	/**
	 * Constructor for Location
	 * @param x x location
	 * @param y y location
	 */
	public Location(int x, int y) {
		this.setX(x); 
		this.setY(y);
	}

	
	/**
	 * Constructor for Location
	 */
	public Location() {
		this(0,0);
	}
	
	/**
	 * Copy Constructor for Location with offsets
	 * @param loc
	 * @param xOffset how much offset will be added to x
	 * @param yOffset how much offset will be added to y
	 */
	public Location(Location loc, int xOffset, int yOffset) {
		this(loc.getX()+xOffset, loc.getY()+yOffset);
	}




	/**
	 * @return the x
	 */
	public synchronized int getX() {
		return x;
	}

	/**
	 * @param x the value to set x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public synchronized int getY() {
		return y;
	}

	/**
	 * @param y the value to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Location [x=" + x + ", y=" + y + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
	
}
