package au.edu.unisa.snake.entities;

import java.awt.Color;
import java.io.Serializable;

import au.edu.unisa.snake.Server;

public class Fruit extends Entity implements Serializable, Runnable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6016944005919098901L;
	private FruitType type;
	private Color color;
	/**
	 * @author saketpopi
	 * setting color for the fruits
	 * 
	 */
	public Fruit(Location location, FruitType type) {
		super(location,true);
		this.type = type;
		
		switch(type){
		case APPLE:
			this.setColor(Color.RED);    
			break;
		case ORANGE:
			this.setColor(Color.ORANGE);   

			break;
		case STRAWBERRY:
			this.setColor(Color.PINK);     

			break;
		default:
			break;
		
		}
	}
	
	/**
	 * @author saketpopi
	 * Returns the score of the fruit
	 * 
	 * @return score
	 */
	public int getScore() {
		return this.type.getValue();
	}
	
	/**
	 *@author saketpopi
	 *Returns the fruit type
	 *
	 * @return fruit type
	 */

	public FruitType getType() {
		return type;
	}
	
	/**
	 * @author saketpopi
	 * Set the fruit type
	 * @param type
	 */
	
	public void setType(FruitType type) {
		this.type = type;
	}

	/**
	 * @author saketpopi
	 * Returns the color for fruit
	 * 
	 * @return color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the color for fruit
	 * 
	 * @param color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void run() {
		if (Thread.currentThread().isInterrupted()) {
			return;
		}
		if (!isAlive()) {
			Thread.currentThread().interrupt();
			return;
		}
		try {
			Thread.sleep(30000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Server.removeFruit(this);
	}
	
	 
	
	
}
