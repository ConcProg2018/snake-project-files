package au.edu.unisa.snake.entities;

public enum Direction {
	UP, DOWN, LEFT, RIGHT, STOP;
	
	public Direction Opposite() {
		switch(this) {
		case DOWN:
			return UP;
		case LEFT:
			return RIGHT;
		case RIGHT:
			return LEFT;
		case UP:
			return DOWN;
		default:
			return STOP;
		}
	}
}
