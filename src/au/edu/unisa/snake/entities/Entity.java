package au.edu.unisa.snake.entities;

import java.io.Serializable;
import java.util.UUID;

public class Entity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8375277329282941878L;

	private  UUID uuid;
	private Location location;
	private boolean isAlive;
	
	public Entity(Location location, boolean isAlive) {
		uuid = UUID.randomUUID();
		this.location = location;
		this.isAlive = isAlive;
	}

	public Entity(Location location) {
		this(location, true);
	}

	public Entity() {
		this(new Location(0, 0), true);
	}
	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @return the isAlive
	 */
	public boolean isAlive() {
		return isAlive;
	}

	/**
	 * @param isAlive the isAlive to set
	 */
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @see java.util.UUID#randomUUID()
	 * @return the uuid that was randomly generated
	 */
	public UUID getUniqueID() {
		return uuid;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Entity [uuid=" + uuid + ", location=" + location + ", isAlive=" + isAlive + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	
	
}
