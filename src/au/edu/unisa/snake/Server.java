package au.edu.unisa.snake;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import au.edu.unisa.snake.entities.*;
import au.edu.unisa.snake.server.ServerEvents;

public class Server extends JFrame {

	private static final long serialVersionUID = -8344267719786988616L;

	public static boolean debugMode = false;
	
	//Server Variables
	public static ServerEvents eventListener;
	public static Server instance;
	
	final static int MAX_PLAYERS = 100;

	private static final DB db = DBMaker.fileDB(new File("snakedb.db")).closeOnJvmShutdown().make();
	private static HTreeMap<String, User> map = db.hashMap("map", String.class, User.class).createOrOpen();

	private static ConcurrentHashMap<UUID, User> users = new ConcurrentHashMap<UUID, User>();
	private static ConcurrentHashMap<UUID, Snake> snakes = new ConcurrentHashMap<UUID, Snake>();
	private static ConcurrentHashMap<UUID, Fruit> fruits = new ConcurrentHashMap<UUID, Fruit>();


	private static User clientUser;
	private static Snake clientPlayer;
	
	//Client Variables
	public static Canvas gameboard;
	public static JPanel buttons;
	public static Graphics graph = null;
	public static BufferStrategy strategy = null;

	private static final int SIZE = 800; // Gameboard Size
	private static final int ESIZE = SIZE / 100; // Square Size/Entity Size

	private static Long frameCounter = 0L;

	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
		for (String string : args) {
			if (string.equals("-debug")) {
				debugMode = true;
			}
		}
		new Server(true);
	}
	
	/**
	 * Server Constructor - Sets everything up
	 * @param showGameboard
	 */
	public Server(boolean showGameboard) {
		instance = this;
		eventListener = new ServerEvents();
		if(showGameboard) {
			gameboard = new Canvas();
			buttons = new JPanel();
  		Runnable users = () -> {
  			createUsersIfAbsent();
  		};
  		Runnable init = () -> {
  			init();
  		};
  		
  		new Thread(users).start();
  		new Thread(init).start();
		}
	}

	/**
	 * Server Constructor
	 */
	public Server() {
		this(false);
	}
	
	/**
	 * Creates a list of users for the database
	 */
	public void createUsersIfAbsent() {
		// Generate Users if Absent
		map.putIfAbsent("JohnSmith", new User("JohnSmith", ""));
		map.putIfAbsent("Jack", new User("Jack", ""));
		map.putIfAbsent("Jill", new User("Jill", ""));
		map.putIfAbsent("Admin", new User("Admin", ""));

		map.putIfAbsent("Matthew", new User("Matthew", ""));
		map.putIfAbsent("Saket", new User("Saket", ""));
		map.putIfAbsent("Nasrin", new User("Nasrin", ""));
		map.putIfAbsent("Chao", new User("Chao", ""));	
		db.commit();
	}
	
	/**
	 * Initiates the GameBoard
	 */
	public void init() {
		setSize(SIZE+6, SIZE+50);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setupButtonBar();
		setupGameboard();
		Runnable initGame = () -> {
			initGame();
		};
		new Thread(initGame).start();
		renderGame();
	}

	/**
	 * Sets up the button bar
	 * @author shimy021
	 */
	public void setupButtonBar() {
		buttons.setSize(SIZE, 50);
		buttons.setLayout(new GridBagLayout());
		buttons.setFocusable(false);
		buttons.setBackground(Color.decode("#383838"));
		Border border = BorderFactory.createEtchedBorder();
		buttons.setBorder(border);
		add(buttons, BorderLayout.NORTH);
		GridBagConstraints gbc = new GridBagConstraints();
		JButton loginButton = new JButton("Login");
		loginButton.setFocusable(false);
		loginButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Thread loginThread = new Thread(new Runnable() {

					@Override
					public void run() {
						LoginBox box = new LoginBox(Server.instance);
						box.setVisible(true);

						if (box.isSucceeded()) {
							clientUser = box.getUser();
							clientPlayer = new Snake(randomLocation(), clientUser.getUsername(), true);
							clientUser.setSnake(clientPlayer);
							Thread thread = new Thread(clientPlayer);
							snakes.put(clientPlayer.getUniqueID(), clientPlayer);
							thread.start();
					        Component c = (Component)e.getSource();
					        c.setVisible(false);
					        c.getParent().revalidate();
					        c.getParent().getComponent(1).setVisible(true);
					        
						} else {
							System.exit(0);
						}
					}

				});
				loginThread.start();

			}
		});


		buttons.add(loginButton, gbc);


		JButton logoutButton = new JButton("Logout");
		logoutButton.setFocusable(false);
		logoutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread logoutThread = new Thread(new Runnable() {
			
					@Override
					public void run() {
						Runnable task = () -> {
							if(clientUser != null ) {
								eventListener.onUserLogoutEvent(clientUser, instance);
								clientUser= null;
								clientPlayer = null;
							}
							loginButton.setVisible(true);
							logoutButton.setVisible(false);
						};
						new Thread(task).start();
					}
				});
				logoutThread.start();
			}
		});

		buttons.add(logoutButton, gbc);
		logoutButton.setVisible(false);
		if(debugMode) {
			JButton increase = new JButton("Add Length");
			increase.setFocusable(false);
			increase.addActionListener(new ActionListener() {
	
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Thread thread = new Thread(new Runnable() {
	
						@Override
						public void run() {
							Server.getClientPlayer().addLength(1);
						}
	
					});
					thread.start();
					
					
				}
			});
	
			buttons.add(increase, gbc);
			
		}
	}

	/**
	 * Sets up the gameboard
	 * @author shimy021
	 */
	public void setupGameboard() {

		gameboard.setSize(new Dimension(SIZE, SIZE));
		add(gameboard, BorderLayout.SOUTH);

		gameboard.setFocusable(true);
		gameboard.addKeyListener(eventListener);
		dispose();
		validate();
		setTitle("Snake");
		setVisible(true);

		gameboard.setIgnoreRepaint(true);
		gameboard.setBackground(Color.WHITE);
		gameboard.createBufferStrategy(2);

		strategy = gameboard.getBufferStrategy();
		graph = strategy.getDrawGraphics();
	}

	/**
	 * Initiates the game by spawning Fruits, Snakes and starts the main loop
	 * @author shimy021
	 */
	public void initGame() {
		for (int i = 0; i < 99; i++) {
			User user = eventListener.onUserLoginEvent("Snake"+i, "password", null);
			eventListener.onPlayerJoinEvent(user);
		}
		spawnFruit();
		mainLoop();		
	}
	
	/**
	 * Renders the gameboard, showing the snakes and fruit (and gridline if debug mode is enabled)
	 * @author shimy021
	 * @author torny002 - Snake Flashing Colour
	 * @author Saket - Fruit Spawning
	 */
	public synchronized void renderGame() {
		gameboard.paint(graph);
		graph = strategy.getDrawGraphics();

		graph.setColor(Color.decode("#0d0d0d"));
		graph.fillRect(0, 0, SIZE, SIZE);

		if (debugMode) {
			Graphics2D g2 = (Graphics2D) graph;
			for (int i = 1; i < SIZE / ESIZE; i++) {
				int x = i * ESIZE;
				g2.setPaint(Color.decode("#333333"));
				g2.drawLine(x, 0, x, getSize().height);
			}
			
			for (int i = 1; i < SIZE / ESIZE; i++) {
				int y = i * ESIZE;
				g2.setPaint(Color.decode("#333333"));
				g2.drawLine(0, y, getSize().width, y);
			}
		}
		Iterator<Entry<UUID, Fruit>> fruits = Server.getFruits().entrySet().iterator();

		while (fruits.hasNext()) {

			Map.Entry<UUID, Fruit> pair = fruits.next();

			Fruit fruit = pair.getValue();
			if (!fruit.isAlive()) {
				continue;
			}
			graph.setColor(fruit.getColor());

			graph.fillRect(fruit.getLocation().getX() * 8+1, fruit.getLocation().getY() * 8+1, ESIZE-2, ESIZE-2);
		}

		Iterator<Entry<UUID, Snake>> it = Server.getSnakes().entrySet().iterator();
		
		
		
		while (it.hasNext()) {
			
			Map.Entry<UUID, Snake> pair = it.next();

			Snake snake = pair.getValue();
			if (!snake.isAlive()) {
				continue;
			}
			for (Location loc : snake.getTail()) {
				graph.setColor(Color.WHITE);
				graph.fillRect(loc.getX() * 8+1, loc.getY() * 8+1, ESIZE-2, ESIZE-2);
			}
			
			if (snake.equals(clientPlayer)) {
				int tenFrame = (int) (frameCounter%300);
				if (tenFrame < 100) {
					graph.setColor(Color.CYAN);
				} else if (tenFrame < 200) {
					graph.setColor(Color.GREEN);
				}else {
					graph.setColor(Color.MAGENTA);
				}
			} else {
				graph.setColor(Color.GRAY);
			}

			graph.fillRect(snake.getLocation().getX() * 8+1, snake.getLocation().getY() * 8+1, ESIZE-2, ESIZE-2);
		}

		strategy.show();
		Toolkit.getDefaultToolkit().sync();
		frameCounter++;
		
	}
	
	/**
	 * Spawns fruit into the game every 10 seconds with a max of 6 at one time
	 * Fruit will despawn after 30 seconds of being itself being on the gameboard
	 * @author Saket
	 */
	public synchronized void spawnFruit() {
		Thread fruitThread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = fruits.size(); i < 6; i ++) {
					Random random = new Random();
					FruitType fruitType = FruitType.values()[random.nextInt(FruitType.values().length)];
					Fruit fruit = new Fruit(randomLocation(),fruitType);
					addFruit(fruit);
				}
				try {
					Thread.sleep(10000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				run();
			}
		}
				);
		fruitThread.start();
	}

	/**
	 * Main game loop, renders the gameboard
	 * @author shimy021
	 */
	public void mainLoop() {
		Runnable loop = () -> {
			while (true) {
				renderGame();
			}
		};
		new Thread(loop).start();;
	}
	
	/**
	 * Checks if the given location is free
	 * <br/><br/>
	 * If it's not free it will return an Entity that's in the location, otherwise it will return null
	 * @param location
	 * @return Entity that fills the location
	 */
	public static synchronized Entity isLocationFree(Location location) {
		//Iterate over all snakes and check their locations against location
		Iterator<Entry<UUID, Snake>> snakes = Server.getSnakes().entrySet().iterator();
		while (snakes.hasNext()) {
			Map.Entry<UUID, Snake> pair = snakes.next();
			Snake snake = pair.getValue();
			if (!snake.isAlive())
				continue;
			if (snake.getLocation().equals(location)) {
				return snake;
			}
			for (Location loc : snake.getTail()) {
				if (loc.equals(location)) {
					return snake;
				}
			}
		}
		
		//Iterate over all fruits and check their location against location
		Iterator<Entry<UUID, Fruit>> fruits = Server.getFruits().entrySet().iterator();
		while (fruits.hasNext()) {
			Map.Entry<UUID, Fruit> pair = fruits.next();
			Fruit fruit = pair.getValue();
			if (fruit.getLocation().equals(location)) {
				return fruit;
			}
		}
		return null;	
	}
	

	/**
	 * Login or Register a user based on a database.
	 * <br/>
	 * If they exist in the database the password must match, if they aren't in the database they are created and joined
	 * @param username
	 * @param password
	 * @return User if Exists/Created, null otherwise
	 */
	public synchronized User userLogin(String username, String password) {

		User user = null;
		if (debugMode) {
			System.out.println("User Login: " + username);
		}


		if (map.containsKey(username)) {
			User tempUser = map.get(username);
			if (tempUser != null) {
				if (tempUser.getPassword().equals(password)) {
					if (debugMode) {
						System.out.println("Password is correct " + password);
					}
					user = tempUser;
				}
				
			}
		} else {
			user = new User(username, password);
			map.put(username, user);
			if (debugMode) {
				System.out.println("Added new user " + user.toString());
			}
		}
		if (debugMode) {
			System.out.println(username + " finished Login");
		}
		return user;
	}
	
	/**
	 * @return the window size
	 */
	public static int getWindowSize() {
		return SIZE;
	}

	/**
	 * @return the Gameboard Size
	 */
	public static int getGameboardSize() {
		return SIZE / ESIZE;
	}

	/**
	 * @return the Entity Size
	 */
	public static int getEntitySize() {
		return ESIZE;
	}


	/**
	 * 
	 * @return the Client Snake
	 */
	public static Snake getClientPlayer() {
		return clientPlayer;
	}

	/**
	 * 
	 * @return the Client User
	 */
	public static User getClientUser() {
		return clientUser;
	}

	/**
	 * @param the Client Snake
	 */
	public static void setClientPlayer(Snake player) {
		clientPlayer = player;
	}

	/**
	 * Logs out the user
	 * @author torny002
	 */
	public synchronized User userLogout(User user) {
		if (user == null) {
			return null;
		}
		if (map.containsKey(user.getUsername())) {
			users.remove(user.getUUID());
		}	
		return user;	
	}

	/**
	 * Generates a random location for an Entity to use
	 * @return random location
	 */
	public static Location randomLocation() {
		Random random = new Random();
		int x = random.nextInt(SIZE / ESIZE);
		Random random2 = new Random();
		int y = random2.nextInt(SIZE / ESIZE);
		return new Location(x, y);
	}

	/**
	 * Gets the Database
	 * @return
	 */
	public DB getDb() {
		return db;
	}

	/**
	 * @return  all users in the game
	 */
	public ConcurrentHashMap<UUID, User> getUsers() {
		return users;
	}

	/**
	 * @param uuid
	 * @return a user from their id
	 */
	public synchronized User getUser(UUID uuid) {
		return users.get(uuid);
	}

	/**
	 * Adds user to server
	 * @param user
	 * @return the user added or null if it couldn't be added
	 */
	public synchronized User addUser(User user) {
		if (user == null || users.containsKey(user.getUUID())) {
			return null;
		}
		users.put(user.getUUID(), user);
		if (users.containsKey(user.getUUID())) {
			return user;
		} else {
			return null;
		}
	}

	/**
	 * Adds a user to the database
	 * @param user
	 * @return the user added or null if it couldn't be added
	 */
	public synchronized User createUser(User user) {
		if (map.containsKey(user.getUsername())) {
			return null;
		}
		return map.putIfAbsent(user.getUsername(), user);
	}

	/**
	 * Delets user from database
	 * @param user
	 * @return the user deleted or null if not exists
	 */
	public synchronized User deleteUser(User user) {
		if (map.containsKey(user.getUsername())) {
			return null;
		}
		return map.remove(user.getUsername());
	}

	/**
	 * Removes user from server
	 * @param user
	 * @return User removed from Server
	 */
	public synchronized User removeUser(User user) {
		if (!users.containsKey(user.getUUID())) {
			return null;
		}
		users.remove(user.getUUID());
		if (users.contains(user.getUUID())) {
			return null;
		} else {
			return user;
		}
	}

	public static ConcurrentHashMap<UUID, Snake> getSnakes() {
		return snakes;
	}


	public static synchronized Snake getSnake(UUID uuid) {
		return snakes.get(uuid);
	}


	public static synchronized Snake addSnake(Snake snake) {
		if (snakes.containsKey(snake.getUniqueID())) {
			return null;
		}
		snakes.put(snake.getUniqueID(), snake);
		return snake;
	}

	public static synchronized Snake removeSnake(Snake snake) {

		if (!snakes.containsKey(snake.getUniqueID())) {
			return null;
		}
		snake.setAlive(false);
		snakes.remove(snake.getUniqueID());
		return snake;
	}

	/**
	 * @author saketpopi
	 * Gets a map of fruits
	 * 
	 * @return fruits
	 */
	public static ConcurrentHashMap<UUID, Fruit> getFruits() {
		return fruits;
	}
	
	/**
	 * @author saketpopi
	 * Sets the fruits into the hashmaps
	 * @param fruits
	 */
	public static void setFruits(ConcurrentHashMap<UUID, Fruit> fruits) {
		Server.fruits = fruits;
	}
	
	/**
	 * @author saketpopi
	 * Get the fruit fruit uuid
	 * @param uuid
	 * @return uuid
	 */
	public static synchronized Fruit getFruit(UUID uuid) {
		return fruits.get(uuid);
	}
	
	/**
	 * @author saketpopi
	 * Adding the fruit to the game
	 * @param fruit
	 * @return fruit
	 */
	public static synchronized Fruit addFruit(Fruit fruit) {
		if (fruits.containsKey(fruit.getUniqueID())) {
			return null;
		}
		fruits.put(fruit.getUniqueID(), fruit);
		Thread thread = new Thread(fruit);
		thread.start();
		return fruit;
	}
	
	public static synchronized ServerEvents getServerEvents() {
		return eventListener;
	}
	
	
/**
 * @author saketpopi
 * Removing the fruit from the snake
 * @param fruit
 * @return fruit
 */
	public static synchronized Fruit removeFruit(Fruit fruit) {
		if (!fruits.containsKey(fruit.getUniqueID())) {
			return null;
		}
		fruit.setAlive(false);
		fruits.remove(fruit.getUniqueID());
		return fruit;
	}

	

}
