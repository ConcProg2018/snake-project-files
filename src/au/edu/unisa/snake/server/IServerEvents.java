package au.edu.unisa.snake.server;

import java.awt.Component;
import java.util.EventListener;

import au.edu.unisa.snake.entities.*;

public interface IServerEvents extends EventListener {
	
	public User onUserLoginEvent(String username, String password, Component component);
	public User onUserLogoutEvent(User user, Component component); 
	public Snake onPlayerJoinEvent(User user);
	public void onSnakeChangeDirectionEvent(Snake snake, Direction dir);
	public void onSnakeEatEvent(Snake snake,Fruit fruit);
	public void onSnakeDieEvent(Snake snake);
	
}
