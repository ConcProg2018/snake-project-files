package au.edu.unisa.snake.server;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.swing.JOptionPane;

import au.edu.unisa.snake.Server;
import au.edu.unisa.snake.entities.Direction;
import au.edu.unisa.snake.entities.Fruit;
import au.edu.unisa.snake.entities.Snake;
import au.edu.unisa.snake.entities.User;

public class ServerEvents implements IServerEvents, KeyListener {

	ExecutorService executor = Executors.newSingleThreadExecutor();

	public ServerEvents() {
	}

	//Number of users trying to login at one
	int userAttempts = 0;
	/**
	 * Logs in the user
	 * @return User that is logged in, null if any errors occured
	 * @author shimy021
	 */
	@Override
	public synchronized User onUserLoginEvent(final String username, final String password, final Component component) {
	    try {
	        while (userAttempts > 1) {
	          wait();
	        }
	      } catch(Exception ex) {}
	    userAttempts++;
	    Callable<User> task1 = () -> {
			User user = Server.instance.userLogin(username, password);
			if (Server.instance.addUser(user) == null) {
				System.err.println("Error: Cannot add user, may already exist");
				return null;
			}
			return user;
		};
		userAttempts--;
		notifyAll();
		Future<User> future = executor.submit(task1);

		try {
			if (future.get() == null) {
				return null;
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}

		Runnable task = () -> {
			User user = null;
			try {
				user = future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			if (component != null) {
				if (user != null) {
					final User u = user;
					Runnable message = () -> {
					JOptionPane.showMessageDialog(component, "Hello " + u.getUsername() + ", You have logged in!",
							"Login Successful", JOptionPane.INFORMATION_MESSAGE);
					};
					new Thread(message).start();

				} else {
					Runnable message = () -> {
					JOptionPane.showMessageDialog(component, "Incorrect Username or Password!", "Login Unsuccessful",
							JOptionPane.ERROR_MESSAGE);
					};
					new Thread(message).start();
				}
			}
		};
		Thread thread = new Thread(task);
		thread.start();
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * Joining the snake into the game, creates a thread for the snake
	 * @return Snake that is spawned, null if any errors
	 * @author shimy021
	 */
	@Override
	public synchronized Snake onPlayerJoinEvent(User user) {
		Callable<Snake> task1 = () -> {
			Snake snake = new Snake(Server.randomLocation(), user.getUsername());
			if (Server.addSnake(snake) == null) {
				System.err.println("Error: Cannot add user, may already exist");
				return null;
			}
			return snake;
		};
		System.out.println(user.getUsername());
		Future<Snake> future = executor.submit(task1);
		Snake snake = null;
		try {
			snake = future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		new Thread(snake, "Snake " + snake.getUsername()).start();
		return snake;

	}


	/**
	 *  Checking a snake movement to make<br/>
	 *         sure it is not in opposite direction.<br/>
	 *         (cannot move in opposite direction)
	 * @author Nasrin
	 */

	@Override
	public synchronized void onSnakeChangeDirectionEvent(Snake snake, Direction dir) {

		Runnable task = () -> {
			if (snake == null || !snake.isHasMoved()) {

				return;
			}
			if (snake.getDirection() == Direction.UP) {
				if (dir == Direction.DOWN) {
					return;
				}
			}
			if (snake.getDirection() == Direction.DOWN) {
				if (dir == Direction.UP) {
					return;
				}
			}
			if (snake.getDirection() == Direction.RIGHT) {
				if (dir == Direction.LEFT) {
					return;
				}
			}
			if (snake.getDirection() == Direction.LEFT) {
				if (dir == Direction.RIGHT) {
					return;
				}
			}

			snake.setDirection(dir);
			snake.setHasMoved(false);
		};
		executor.execute(task);
	}

	
	/**
	 * Remove the user when it is gonna left the game
	 * @author Nasrin 
	 */

	@Override
	public synchronized User onUserLogoutEvent(User user, Component component) {
		User u = Server.instance.removeUser(user); 
		if (u != null) {
			if (component != null) {
				Server.removeSnake(user.getSnake());
				Runnable message = () -> {
					JOptionPane.showMessageDialog(component, "!!!! " + user.getUsername() + ", You have logged out!",
						"Logout Successful", JOptionPane.INFORMATION_MESSAGE);
				};
				new Thread(message).start();
			}
			return user;
		} else {
			if (component != null) {
				JOptionPane.showMessageDialog(component, " ", "Logout Unsuccessful", JOptionPane.ERROR_MESSAGE);
			}
			return null;
		}
	}

	@Override
	
	/**
	 * When snake eats fruit, remove fruit and add score/length to snake
	 */
	public synchronized void onSnakeEatEvent(Snake snake, Fruit fruit) {
		if (!fruit.isAlive()) {
			Server.removeFruit(fruit);
			return;
		}
		snake.addScore(fruit.getScore());
		snake.addLength(1);
		Server.removeFruit(fruit);
	}
	/**
	 * When snake dies, if it's the client snake prompt a box with options. Kills the snake
	 */

	@Override
	public synchronized void onSnakeDieEvent(Snake snake) {
		Runnable task = () -> {
			Server.removeSnake(snake);
			snake.setAlive(false);

			if (snake.isPlayer()) {
				Runnable task2 = () -> {
					Server.setClientPlayer(null);
					String[] Snakedied = { "Respawn", "Spectate", "Quit" };

					int n = JOptionPane.showOptionDialog(Server.instance,
							"You have died\nScore: " + snake.getScore() + "\n\nPlease select an option below",
							"You Have Died!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,
							Snakedied, Snakedied[0]);
					switch (n) {

					case 0: {
						Server.setClientPlayer(
								new Snake(Server.randomLocation(), Server.getClientUser().getUsername(), true));
						new Thread(Server.getClientPlayer(), "Client Player").start();
						Server.addSnake(Server.getClientPlayer());
						break;
					}
					case 1:
						break;
					case 2:
						System.exit(0);
						break;

					default:
						break;
					}
				};
				Thread thread = new Thread(task2);
				thread.start();
			}
		};
		executor.execute(task);
	}
	

	@Override
	public void keyPressed(KeyEvent event) {

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				Direction dir = Direction.STOP;
				switch (event.getKeyCode()) {
				case KeyEvent.VK_UP:
				case KeyEvent.VK_W:
					dir = Direction.UP;
					break;

				case KeyEvent.VK_DOWN:
				case KeyEvent.VK_S:
					dir = Direction.DOWN;
					break;

				case KeyEvent.VK_LEFT:
				case KeyEvent.VK_A:
					dir = Direction.LEFT;
					break;

				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_D:
					dir = Direction.RIGHT;
					break;
				}

				onSnakeChangeDirectionEvent(Server.getClientPlayer(), dir);
			}
		});
		thread.start();

	}
	

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}
