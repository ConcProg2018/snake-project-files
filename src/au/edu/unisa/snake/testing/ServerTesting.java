package au.edu.unisa.snake.testing;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import au.edu.unisa.snake.Server;
import au.edu.unisa.snake.entities.User;
import au.edu.unisa.snake.server.ServerEvents;

import static org.mockito.Mockito.*;


public class ServerTesting {


	Server server;
	@Before
	public void init() {
		server = new Server(false);
	}
	
	/**
	 * @author torny002
	 * For testing the logging in and out of players, and checking the correct players are added/removed at login/logout
	 */

	@Test
	public void LogoutTest() {


		//Array to stores test users
		User[] logUsers = new User[100];

		//Loging in users and adding them to array
		for(int i=0; i<100; i++) {
			logUsers[i] = Server.eventListener.onUserLoginEvent("user " + i  , "password", null);
		}
		
		//Testing login
		Assert.assertEquals(100,server.getUsers().size(),0);

		//logging users out
		User[] logoutUsers = new User[50];
		for (int i = 0; i < 50; i++) {
			logoutUsers[i] = Server.eventListener.onUserLogoutEvent(logUsers[i], null);	
		}
		Assert.assertEquals(50,server.getUsers().size(),0);
		//Testing other player are still logged in
		for (int i = 0; i < 50; i++) {
			Assert.assertEquals(server.getUser(logUsers[i].getUUID()), null);
		}
	}
	
	
	
	ServerEvents mockEvents = mock(ServerEvents.class, RETURNS_DEEP_STUBS);
	/**
	 * @author matthew
	 * Mock testing login, there wasn't a lot of concurrency we could do testing on...
	 */
	@Test
	public void UserLoginMockTest() {
		User expected = new User("Matthew","password");
		doReturn(expected).when(mockEvents).onUserLoginEvent("Matthew", "password", null);
		User user = mockEvents.onUserLoginEvent("Matthew", "password", null);
        verify(mockEvents).onUserLoginEvent("Matthew", "password", null);
        
		Assert.assertEquals(expected, user);
		System.out.println(user.toString());
	}
}
